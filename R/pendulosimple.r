# correr asi
# Rscript pendulosimple.r

parameters <- c(g = 9.8,l = 1.0,m = 0.1)

state <- c(X = 3.14,V = 0.0)

Pendulo<-function(t, state, parameters) {
 with(as.list(c(state, parameters)),{
 # rate of change
 dX <- V
 dV <- (-g/l)*sin(X)

 # return the rate of change
 list(c(dX, dV))
 }) # end with(as.list ...
}

times <- seq(0, 100, by = 0.01)

library(deSolve)

out <- ode(y = state, times = times, func = Pendulo, parms = parameters) 

head(out)

#par(oma = c(0, 0, 3, 0))
plot(out, xlab = "time", ylab = "-")
plot(out[, "X"], out[, "V"], pch = ".")
mtext(outer = TRUE, side = 3, "Pendulo Simple", cex = 1.5)
