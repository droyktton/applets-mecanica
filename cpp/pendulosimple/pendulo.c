#include<stdio.h>
#include<math.h>

double t,dt,g,l,m;

double Energia(double o,double doto){
	return 0.5*m*l*l*doto*doto-m*g*l*cos(o);
}

double FO(double o){
	return -(g/l)*sin(o);
}

void yoshida(double *O, double *dotO)
{
	// Yoshida parameters
	double aux=pow(2,1.0/3.0);
	double w0=-aux/(2-aux);   //-2**(1./3.)/(2-2**(1./3.));
	double w1=1.0/(2-aux);   //1.0/(2-2**(1./3.));
	double c1=w1*0.5;
	double c4=c1;
	double c2=(w0+w1)*0.5;
	double c3=c2;
	double d1=w1; 
	double d3=d1;
	double d2=w0;

	double X=*O;
	double Vx=*dotO;	
		
	double X1=X+c1*Vx*dt; 
	double Vx1=Vx+d1*dt*FO(X1); 

	double X2=X1+c2*Vx1*dt; 
	double Vx2=Vx1+d2*dt*FO(X2); 

	double X3=X2+c3*Vx2*dt; 
	double Vx3=Vx2+d3*dt*FO(X3); 

	X=X3+c4*Vx3*dt;
	Vx=Vx3;
	
	*O=X;
	*dotO=Vx;
}


int main()
{
	t=0.0;
	dt=0.005;
	m=0.1;
	g=9.8;
	l=1.0;
	
	double E;
	double O,dotO;
	
	dotO=0.0;
	O=3.14;

	for(int i=0;i<5000;i++)
	{
		t+=dt;

		E=Energia(O,dotO);
		yoshida(&O,&dotO);		
				
		printf("%f %f %f %f\n",t,O,dotO,E);	
	}

	return 0;
}
