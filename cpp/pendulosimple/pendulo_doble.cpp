// Instalar la libreria boost odeint
// Para Compilar
// g++ pendulo.cpp

#include <iostream>
#include <boost/array.hpp>

#include <boost/numeric/odeint.hpp>

using namespace std;
using namespace boost::numeric::odeint;

// parametros
const double g = 9.8;
const double l1 = 1.0;
const double l2 = 1.0;
const double m1 = 1.0;
const double m2 = 1.0;

// el estado del pendulo doble tiene 4 variables del tipo "double" (doble precision)
typedef boost::array< double , 4 > state_type;

void pendulo_doble( const state_type &x , state_type &dxdt , double t )
{
    // Pendulo_doble:
 
    // ang0 ==> x[0]
    double a1 =x[0];

    // ang1 ==> x[1]
    double a2 =x[1];

    // velang0 ==> x[0]
    double z1 =x[2];

    // velang1 ==> x[1]
    double z2 =x[3];

    // terminos de la derecha

/*
*/
    double da1dt = z1; // dang0/dt = velang0
    double da2dt = z2;

    double dz1dt = 
        (m2*g*sin(a2)*cos(a1-a2)-m2*sin(a1-a2)*(l1*z1*z1*cos(a1-a2)+l2*z2*z2)-(m1+m2)*g*sin(a1))
        /(l1*(m1+m2*sin(a1-a2)*sin(a1-a2)));

    double dz2dt = 
        ((m1+m2)*(l1*z1*z1*sin(a1-a2)-g*sin(a2)+g*sin(a1)*cos(a1-a2))+m2*l2*z2*z2*sin(a1-a2)*cos(a1-a2))
        /(l2*(m1+m2*sin(a1-a2)*sin(a1-a2)));

    dxdt[0] = da1dt;
    dxdt[1] = da2dt;
    dxdt[2] = dz1dt;
    dxdt[3] = dz2dt;

}

void write_pendulo( const state_type &x , const double t )
{
    // Pendulo_doble: 
    // ang0 ==> x[0]
    double a1 =x[0];

    // ang1 ==> x[1]
    double a2 =x[1];

    // velang0 ==> x[0]
    double z1 =x[2];

    // velang1 ==> x[1]
    double z2 =x[3];

    double V = -(m1+m2)*l1*g*cos(a1) - m2*l2*g*cos(a2);
    double T = 0.5*m1*pow(l1*z1,2.0) + 0.5*m2*(pow(l1*z1,2.0) + pow(l2*z2,2.0) +
            2*l1*l2*z1*z2*cos(a1-a2));

    double Energia=T+V;

    cout << t 
    << '\t' << a1 << '\t' << a2 
    << '\t' << z1 << '\t' << z2 
    << '\t' << l1*sin(a1) << '\t' << l1*sin(a1)+l2*sin(a2) 
    << '\t' << -l1*cos(a1) << '\t' << -l1*cos(a1)-l2*cos(a2) 
    << " " << Energia << endl;
}

int main(int argc, char **argv)
{
    //typedef symplectic_rkn_sb3a_mclachlan< state_type > stepper_type;

    double a1=atof(argv[1]);
    double a2=atof(argv[2]);
    double z1=atof(argv[3]);
    double z2=atof(argv[4]);
    double trun=atof(argv[5]);

    state_type x = { a1, a2, z1, z2}; // condiciones iniciales {ang0,velang0, ang1, velang1}
    integrate( pendulo_doble , x , 0.0 , trun , 0.01 , write_pendulo );    
    
    // variante1
    //runge_kutta4< state_type > stepper;
    //integrate_const( stepper, pendulo_doble , x , 0.0 , trun , 0.01 , write_pendulo );

    // variante2
    /*typedef runge_kutta_cash_karp54< state_type > error_stepper_type;
    typedef controlled_runge_kutta< error_stepper_type > controlled_stepper_type;
    controlled_stepper_type controlled_stepper;
    integrate_adaptive( controlled_stepper , pendulo_doble  , x , 0.0 , trun , 0.01 , write_pendulo );
    */
}
