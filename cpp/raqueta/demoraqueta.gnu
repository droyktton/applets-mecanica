res
unset multi
system("rm peli*png");

set term qt 0 size 400,600

traza=50
pausita=0.01
step=10
trun=50000

#movie=0
#recalcular=1;

#set terminal gif transparent nocrop enhanced animate size 640,800 
if(movie==1) {set term png size 640,700 notransparent}

j=0;

set xla 'X'
set yla 'Y'
set zla 'Z'


comando(w1,w2,w3,trun)=sprintf("./a.out %f %f %f 0.001 0.001 0.001 1 1.5 3 0.01 %d > zzz",w1,w2,w3,trun);


if(recalcular==1){

if(modo==0){
print "modo0"
print comando(0.0001,0.0001,0.0001,50)
system(comando(0.0001,0.0001,0.0001,50)); 
system("mv posiciones.dat posiciones0.dat; mv euler.dat euler0.dat");
}

if(modo==1){
print "modo3"
print comando(0.0001,0.0001,1,trun);
system(comando(0.0001,0.0001,1,trun)); 
system("mv posiciones.dat posiciones1.dat; mv euler.dat euler1.dat");
}

if(modo==2){
print "modo2"
print comando(0.0001,1,0.0001,trun);
system(comando(0.0001,1,0.0001,trun)); 
system("mv posiciones.dat posiciones2.dat; mv euler.dat euler2.dat");
}

if(modo==3){
print "modo1"
print comando(1, 0.0001,0.0001,trun);
system(comando(1, 0.0001,0.0001,trun)); 
system("mv posiciones.dat posiciones3.dat; mv euler.dat euler3.dat");
}

if(modo==4){
print "modox"
print comando(1, 1, 1,trun);
system(comando(2, 2,2,trun)); 
system("mv posiciones.dat posicionesx.dat; mv euler.dat eulerx.dat");
}
}


print "listo... empieza la animacion"

if(modo==0){
set tit "w1=0.0001, w2=0.0001, w3=0.0001, psi=0.01, theta=0.01, phi=0.01, I1=1, I2=1.5, I3=5"
do for[i=0:0:step]{

    if(movie==1) {set out "peli".j.".png"; j=j+1;}

    splot [-1:1][-1:1][-1:1] 'posiciones0.dat' u 1:2:3 index i:(i) w lp lw 3 ps 4 pt 7 t 'Quieto',\
    ''  u 1:2:3 index ((i>=traza)?(i-traza):(0)):(i) w p ps 1 pt 6 t ''

}
if(movie==0){pause mouse;}
}

if(modo==1){
set tit "w1=0.0001, w2=0.0001, w3=1, psi=0.01, theta=0.01, phi=0.01, I1=1, I2=1.5, I3=5"
do for[i=0:trun:step]{

    if(movie==1) {set out "peli".j.".png"; j=j+1;}

    com=sprintf("awk 'NR==%d{print $1,$2,$3,$4,$5,$6}' euler1.dat",i)
    vectores=system(com);
    set arrow from 0,0,0 to word(vectores,1),word(vectores,2),word(vectores,3) lw 4

    splot [-1:1][-1:1][-1:1] 'posiciones1.dat' u 1:2:3 index i:(i) w lp lw 3 ps 4 pt 7 t 'Eje 3 [Mayor]',\
    ''  u 1:2:3 index ((i>=traza)?(i-traza):(0)):(i) w p ps 1 pt 6 t ''
 
    pause pausita; 

    unset arrow

}
if(movie==0){pause mouse;}
}

if(modo==2){
set tit "w1=0.0001, w2=1, w3=0.0001, psi=0.01, theta=0.01, phi=0.01, I1=1, I2=1.5, I3=5"
do for[i=0:trun:step]{

    if(movie==1) {set out "peli".j.".png"; j=j+1;}

    com=sprintf("awk 'NR==%d{print $1,$2,$3,$4,$5,$6}' euler2.dat",i)
    vectores=system(com);
    set arrow from 0,0,0 to word(vectores,1),word(vectores,2),word(vectores,3) lw 4

    splot [-1:1][-1:1][-1:1] 'posiciones2.dat' u 1:2:3 index i:(i) w lp lw 3 ps 4 pt 7 t 'Eje 2 [Intermedio]',\
    ''  u 1:2:3 index ((i>=traza)?(i-traza):(0)):(i) w p ps 1 pt 6 t ''

    unset arrow

    pause pausita;
}
if(movie==0){pause mouse;}
}


if(modo==3){
set tit "w1=1, w2=0.0001, w3=0.0001, psi=0.01, theta=0.01, phi=0.01, I1=1, I2=1.5, I3=5"
do for[i=0:trun:step]{

    if(movie==1) {set out "peli".j.".png"; j=j+1;}

    com=sprintf("awk 'NR==%d{print $1,$2,$3,$4,$5,$6}' euler3.dat",i)
    vectores=system(com);
    set arrow from 0,0,0 to word(vectores,1),word(vectores,2),word(vectores,3) lw 4

    splot [-1:1][-1:1][-1:1] 'posiciones3.dat' u 1:2:3 index i:(i) w lp lw 3 ps 4 pt 7 t 'Eje 1 [Menor]',\
    '' u 1:2:3 index ((i>=traza)?(i-traza):(0)):(i) w p ps 1 pt 6 t ''

    unset arrow

    pause pausita; 
}
if(movie==0){pause mouse;}
}


if(modo==4){
set tit "w1=2, w2=2, w3=2, psi=0.01, theta=0.01, phi=0.01, I1=1, I2=1.5, I3=5"
do for[i=0:trun:step]{

    if(movie==1) {set out "peli".j.".png"; j=j+1;}

    com=sprintf("awk 'NR==%d{print $1,$2,$3,$4,$5,$6}' eulerx.dat",i)
    vectores=system(com);
    set arrow from 0,0,0 to word(vectores,1),word(vectores,2),word(vectores,3) lw 4

    splot [-1:1][-1:1][-1:1] 'posicionesx.dat' u 1:2:3 index i:(i) w lp lw 3 ps 4 pt 7 t 'Mezcla',\
    '' u 1:2:3 index ((i>=traza)?(i-traza):(0)):(i) w p ps 1 pt 6 t ''

    unset arrow

    pause pausita; 
}
if(movie==0){pause mouse;}
}

unset multi
