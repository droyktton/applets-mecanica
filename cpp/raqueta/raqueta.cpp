// Instalar la libreria boost odeint
// Para Compilar
// g++ raqueta.cpp

#include <iostream>
#include <fstream>
#include <boost/array.hpp>

#include <boost/numeric/odeint.hpp>

using namespace std;
using namespace boost::numeric::odeint;

double I1 = 1.2;
double I2 = 1.0;
double I3 = 0.8;
double beta=0.05;

std::ofstream posout("posiciones.dat");
std::ofstream eulerout("euler.dat");
std::ofstream logout("logout.dat");
std::ofstream checkout("check.dat");

typedef boost::array< double , 6 > state_type;

double Power(double x, double p){
    return pow(x,p);
}

void raqueta( const state_type &x , state_type &dxdt , double t )
{
    /* Ojo, nomenclatura tipica intercambiada psi <--> phi*/
    // https://rotations.berkeley.edu/a-tumbling-t-handle-in-space/

    double w1=x[0];
    double w2=x[1];
    double w3=x[2];
    double psi=x[3];
    double theta=x[4];
    double phi=x[5];
    double dw1, dw2, dw3, dpsi, dtheta, dphi;

    // todo lo que viene se podria optimizar mucho ya sea usando opciones adecuadas al compilar
    // o definiendo sin(phi),sin(psi),sin(theta),cos(phi),cos(psi),cos(theta) y sus cuadrados antes

    // dw1/dt, dw2/dt, dw3/dt
    dw1 = ((I2 - I3)*w2*w3*(-(I2*I3*Power(cos(phi),2)*sin(theta)) - 
       I2*I3*Power(sin(phi),2)*sin(theta)))/
   (-(I1*I2*I3*Power(cos(phi),2)*sin(theta)) - 
     I1*I2*I3*Power(sin(phi),2)*sin(theta));

    dw2 = ((-I1 + I3)*w1*w3*(-(I1*I3*Power(cos(phi),2)*sin(theta)) - 
       I1*I3*Power(sin(phi),2)*sin(theta)))/
   (-(I1*I2*I3*Power(cos(phi),2)*sin(theta)) - 
     I1*I2*I3*Power(sin(phi),2)*sin(theta));

    dw3 = ((I1 - I2)*w1*w2*(-(I1*I2*Power(cos(phi),2)*sin(theta)) - 
       I1*I2*Power(sin(phi),2)*sin(theta)))/
   (-(I1*I2*I3*Power(cos(phi),2)*sin(theta)) - 
     I1*I2*I3*Power(sin(phi),2)*sin(theta));

    // dpsi/dt, dtheta/dt, dphi/dt
    dpsi = -((I1*I2*I3*w2*cos(phi))/
      (-(I1*I2*I3*Power(cos(phi),2)*sin(theta)) - 
        I1*I2*I3*Power(sin(phi),2)*sin(theta))) - 
   (I1*I2*I3*w1*sin(phi))/
    (-(I1*I2*I3*Power(cos(phi),2)*sin(theta)) - 
      I1*I2*I3*Power(sin(phi),2)*sin(theta));

    dtheta = -((I1*I2*I3*w1*cos(phi)*sin(theta))/
      (-(I1*I2*I3*Power(cos(phi),2)*sin(theta)) - 
        I1*I2*I3*Power(sin(phi),2)*sin(theta))) + 
   (I1*I2*I3*w2*sin(phi)*sin(theta))/
    (-(I1*I2*I3*Power(cos(phi),2)*sin(theta)) - 
      I1*I2*I3*Power(sin(phi),2)*sin(theta));

    dphi = w3 + (I1*I2*I3*w2*cos(phi)*cos(theta))/
    (-(I1*I2*I3*Power(cos(phi),2)*sin(theta)) - 
      I1*I2*I3*Power(sin(phi),2)*sin(theta)) + 
   (I1*I2*I3*w1*cos(theta)*sin(phi))/
    (-(I1*I2*I3*Power(cos(phi),2)*sin(theta)) - 
      I1*I2*I3*Power(sin(phi),2)*sin(theta));

    dxdt[0]=dw1-beta*w1;
    dxdt[1]=dw2-beta*w2;
    dxdt[2]=dw3-beta*w3;
    dxdt[3]=dpsi;
    dxdt[4]=dtheta;
    dxdt[5]=dphi;
}

void write_raqueta( const state_type &x , const double t )
{

    double w1=x[0];
    double w2=x[1];
    double w3=x[2]; 
    double psi=x[3];
    double theta=x[4];
    double phi=x[5]; 


    cout << t << '\t';
    for(int i=0;i<6;i++)
    cout << x[i] << '\t';
    cout << std::endl;

    /* Esto seria mucho mas simple usando una biblioteca de matrices */ 

    double vers1x = cos(phi)*cos(psi) - cos(theta)*sin(phi)*sin(psi);
    double vers1y = cos(psi)*cos(theta)*sin(phi) + cos(phi)*sin(psi);
    double vers1z = sin(phi)*sin(theta);

    double vers2x = -(cos(psi)*sin(phi)) - cos(phi)*cos(theta)*sin(psi);
    double vers2y = cos(phi)*cos(psi)*cos(theta) - sin(phi)*sin(psi);
    double vers2z = cos(phi)*sin(theta);

    double vers3x = sin(psi)*sin(theta);
    double vers3y = -(cos(psi)*sin(theta));
    double vers3z = cos(theta);

    // vectorialmente
    // pos1= vers1
    // pos2= -vers1
    // pos3= vers2   
    double pos1x=vers1x; double pos1y=vers1y; double pos1z=vers1z;
    double pos2x=-vers1x; double pos2y=-vers1y; double pos2z=-vers1z;
    double pos3x=vers2x; double pos3y=vers2y; double pos3z=vers2z;
  
    // imprimimos el cuerpo
    posout << pos1x << '\t' << pos1y << '\t' << pos1z << '\n';
    posout << pos2x << '\t' << pos2y << '\t' << pos2z << '\n';
    posout << 0 << '\t' << 0 << '\t' << 0 << '\n';
    posout << pos3x << '\t' << pos3y << '\t' << pos3z << '\n';
    posout << "\n\n";
 
    /* check: rotacion debe preservar las distancias */
    double Energia =(I1*w1*w1+I1*w2*w2+I1*w3*w3)/2.0;
    double MomAngx = I1*w1*vers1x+I2*w2*vers2x+I3*w3*vers3x;
    double MomAngy = I1*w1*vers1y+I2*w2*vers2y+I3*w3*vers3y;
    double MomAngz = I1*w1*vers1z+I2*w2*vers2z+I3*w3*vers3z;

    checkout << "d12^2= " << Power(pos1x-pos2x,2.0)+Power(pos1y-pos2y,2.0)+Power(pos1z-pos2z,2.0)<< std::endl;
    checkout << "d13^2= " << Power(pos1x-pos3x,2.0)+Power(pos1y-pos3y,2.0)+Power(pos1z-pos3z,2.0)<< std::endl;
    checkout << "d23^2= " << Power(pos2x-pos3x,2.0)+Power(pos2y-pos3y,2.0)+Power(pos2z-pos3z,2.0)<< std::endl;
    checkout << "Energia= " << Energia << " " << I1*w1*w1 << std::endl;
    checkout << "Momangx= " << MomAngx << std::endl;
    checkout << "Momangy= " << MomAngy << std::endl;
    checkout << "Momangz= " << MomAngz << std::endl;


    for(int i=0;i<6;i++) eulerout << x[i] << "\t";
    eulerout << Energia << " " << MomAngx << " " << MomAngy << " " << MomAngz << "\n";

}

int main(int argc, char **argv)
{
    cout << "t w1 w2 w3 psi theta phi"  << std::endl;
 //x1 y1 z1 x2 y2 z2 x3 y3 z3

    double w1=atof(argv[1]); 
    double w2=atof(argv[2]);
    double w3=atof(argv[3]);
    double psi=atof(argv[4]);
    double theta=atof(argv[5]);
    double phi=atof(argv[6]);
    I1 = atof(argv[7]);
    I2 = atof(argv[8]);
    I3 = atof(argv[9]);
    beta = atof(argv[10]);
    double tot = atof(argv[11]);

    logout << "w1=" << w1 << ", w2=" << w2 << ", w3=" << w3 
    << ", psi=" << psi << ", theta=" << theta << ", phi=" << phi 
    << ", I1=" << I1 << ", I2=" << I2 << ", I3=" << I3 << ", beta=" << beta << ", tot=" << tot << std::endl;

    state_type x = {w1,w2, w3, psi, theta, phi}; // condiciones iniciales {ang0,velang0}

    runge_kutta4< state_type > stepper;
    integrate_const( stepper, raqueta , x , 0.0 , tot , 0.01 , write_raqueta );
}
