*     Pendulo simple resuelto con Yoshida 4to orden
*	
*     si el ejecutable se llama a.out
*     gnuplot> set xla 't'; plot '< ./a.out' u 1:2, '' u 1:3, '' u 1:4, 0
*     para visualizar la solucion con gnuplot

      PROGRAM PENDULO
	integer i,n
	real G,L,M,X,V,t,dt
	
	n=5000
	g=9.8
	l=1.0
	m=0.1
	dt=0.005

c        PRINT *, 'O(0)=?'
c       READ *, X

c        PRINT *, 'dO/dt(0)=?'
c        READ *, V		

c	Condiciones iniciales
	X=3.14
	V=0.0

	t=0
	DO 10 i=1,n
		E=Energia(X,V,M,L,G)

C		metodo de segundo orden
C		CALL Leapfrog(X,V,dt,M,L,G)

C		cuarto de cuarto orden
		CALL Yoshida(X,V,dt,M,L,G)
		
		t=t+dt	

		PRINT *, t, X, V, E
10	CONTINUE		
        STOP
      END
            
      real FUNCTION Energia(x,v,m,l,g)
      	real g,l,m,x,v
	Energia=0.5*m*l*l*v*v - m*g*l*cos(x)
      return
      END
      
      real FUNCTION Force(x,l,g)
      	real g,l,x
	Force=-(g/l)*sin(x)
      return
      END

      SUBROUTINE Yoshida(x,v,dt,m,l,g)
      	real aux, w0, w1, c1, c2, c3, c4, d1, d2, d3
      	real X0,V0,X1,V1,X2,V2,X3,V3,x,v,m,l,g
      	
C	parametros       
	aux=2.0**(1.0/3.0)
	w0=-aux/(2-aux)  
	w1=1.0/(2-aux)   
	c1=w1*0.5
	c4=c1
	c2=(w0+w1)*0.5
	c3=c2
	d1=w1 
	d3=d1
	d2=w0
		
	X0=x
	V0=v
		
	X1=X0+c1*V0*dt; 
	V1=V0+d1*dt*Force(X1,l,g) 

	X2=X1+c2*V1*dt; 
	V2=V1+d2*dt*Force(X2,l,g) 

	X3=X2+c3*V2*dt; 
	V3=V2+d3*dt*Force(X3,l,g)

	x=X3+c4*V3*dt
	v=V3
	
        return
      END	     
      
      SUBROUTINE Leapfrog(x,v,dt,m,l,g)
      	real x,v,dt,m,l,g
C	v=v+dt*(-g/l)*sin(x)
	v=v+dt*Force(x,l,g)
	x=x+dt*v
        return
      end	     
      
      
