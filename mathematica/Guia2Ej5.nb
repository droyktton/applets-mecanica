(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      9335,        311]
NotebookOptionsPosition[      8423,        274]
NotebookOutlinePosition[      8758,        289]
CellTagsIndexPosition[      8715,        286]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"x", "[", "t", "]"}], "=", 
   RowBox[{
    RowBox[{"xp", "[", "t", "]"}], "+", 
    RowBox[{"L", "*", 
     RowBox[{"Sin", "[", 
      RowBox[{"o", "[", "t", "]"}], "]"}]}]}]}], 
  "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.810327539243061*^9, 3.810327597525551*^9}, {
  3.8103276554575872`*^9, 3.8103277039859247`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"L", " ", 
   RowBox[{"Sin", "[", 
    RowBox[{"o", "[", "t", "]"}], "]"}]}], "+", 
  RowBox[{"xp", "[", "t", "]"}]}]], "Output",
 CellChangeTimes->{3.810331561676339*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"y", "[", "t", "]"}], "=", 
   RowBox[{
    RowBox[{"-", "L"}], "*", 
    RowBox[{"Cos", "[", 
     RowBox[{"o", "[", "t", "]"}], "]"}]}]}]}]], "Input",
 CellChangeTimes->{{3.810327663036888*^9, 3.8103277091444798`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "L"}], " ", 
  RowBox[{"Cos", "[", 
   RowBox[{"o", "[", "t", "]"}], "]"}]}]], "Output",
 CellChangeTimes->{{3.810327688391255*^9, 3.810327709795719*^9}, 
   3.81033079819064*^9, 3.8103312974622*^9, {3.810331545712373*^9, 
   3.810331569390077*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"T", "=", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{"M", "/", "2"}], ")"}], "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          SubscriptBox["\[PartialD]", "t"], 
          RowBox[{"x", "[", "t", "]"}]}], ")"}], "^", "2"}], "+", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          SubscriptBox["\[PartialD]", "t"], 
          RowBox[{"y", "[", "t", "]"}]}], ")"}], "^", "2"}]}], ")"}]}], "+", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"m", "/", "2"}], ")"}], "*", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        SubscriptBox["\[PartialD]", "t"], 
        RowBox[{"xp", "[", "t", "]"}]}], ")"}], "^", "2"}]}]}]}]}]], "Input",
 CellChangeTimes->{{3.81032771154548*^9, 3.810327723647348*^9}, {
  3.810330732160694*^9, 3.81033078224358*^9}, {3.8103308264971247`*^9, 
  3.810330863166609*^9}, {3.810331575823601*^9, 3.810331620831057*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   FractionBox["1", "2"], " ", "m", " ", 
   SuperscriptBox[
    RowBox[{
     SuperscriptBox["xp", "\[Prime]",
      MultilineFunction->None], "[", "t", "]"}], "2"]}], "+", 
  RowBox[{
   FractionBox["1", "2"], " ", "M", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      SuperscriptBox["L", "2"], " ", 
      SuperscriptBox[
       RowBox[{"Sin", "[", 
        RowBox[{"o", "[", "t", "]"}], "]"}], "2"], " ", 
      SuperscriptBox[
       RowBox[{
        SuperscriptBox["o", "\[Prime]",
         MultilineFunction->None], "[", "t", "]"}], "2"]}], "+", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{
        RowBox[{"L", " ", 
         RowBox[{"Cos", "[", 
          RowBox[{"o", "[", "t", "]"}], "]"}], " ", 
         RowBox[{
          SuperscriptBox["o", "\[Prime]",
           MultilineFunction->None], "[", "t", "]"}]}], "+", 
        RowBox[{
         SuperscriptBox["xp", "\[Prime]",
          MultilineFunction->None], "[", "t", "]"}]}], ")"}], "2"]}], 
    ")"}]}]}]], "Output",
 CellChangeTimes->{3.810331302592662*^9, 3.810331621624083*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"U", "=", 
  RowBox[{
   RowBox[{"-", "m"}], "*", "g", "*", "L", "*", 
   RowBox[{"Cos", "[", 
    RowBox[{"o", "[", "t", "]"}], "]"}]}]}]], "Input",
 CellChangeTimes->{
  3.8103312754398193`*^9, {3.8103314042144213`*^9, 3.810331422990692*^9}, {
   3.810331629035495*^9, 3.810331635519619*^9}, 3.810331703662921*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "g"}], " ", "L", " ", "m", " ", 
  RowBox[{"Cos", "[", 
   RowBox[{"o", "[", "t", "]"}], "]"}]}]], "Output",
 CellChangeTimes->{3.810331275636314*^9, 3.810331305712018*^9, 
  3.810331423591823*^9, 3.810331636067286*^9, 3.810331704409299*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Lagrangiana", "=", 
  RowBox[{"T", "-", "U"}]}]], "Input",
 CellChangeTimes->{{3.810331316184031*^9, 3.810331318491734*^9}, {
  3.810331437948538*^9, 3.810331450042554*^9}, {3.810331646852874*^9, 
  3.810331649369484*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"g", " ", "L", " ", "m", " ", 
   RowBox[{"Cos", "[", 
    RowBox[{"o", "[", "t", "]"}], "]"}]}], "+", 
  RowBox[{
   FractionBox["1", "2"], " ", "m", " ", 
   SuperscriptBox[
    RowBox[{
     SuperscriptBox["xp", "\[Prime]",
      MultilineFunction->None], "[", "t", "]"}], "2"]}], "+", 
  RowBox[{
   FractionBox["1", "2"], " ", "M", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      SuperscriptBox["L", "2"], " ", 
      SuperscriptBox[
       RowBox[{"Sin", "[", 
        RowBox[{"o", "[", "t", "]"}], "]"}], "2"], " ", 
      SuperscriptBox[
       RowBox[{
        SuperscriptBox["o", "\[Prime]",
         MultilineFunction->None], "[", "t", "]"}], "2"]}], "+", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{
        RowBox[{"L", " ", 
         RowBox[{"Cos", "[", 
          RowBox[{"o", "[", "t", "]"}], "]"}], " ", 
         RowBox[{
          SuperscriptBox["o", "\[Prime]",
           MultilineFunction->None], "[", "t", "]"}]}], "+", 
        RowBox[{
         SuperscriptBox["xp", "\[Prime]",
          MultilineFunction->None], "[", "t", "]"}]}], ")"}], "2"]}], 
    ")"}]}]}]], "Output",
 CellChangeTimes->{3.810331319286416*^9, 3.810331450831801*^9, 
  3.8103315128454027`*^9, 3.810331650412179*^9, 3.810331705733611*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"Needs", "[", "\"\<VariationalMethods`\>\"", "]"}]}]], "Input",
 CellChangeTimes->{{3.810331028386724*^9, 3.810331028850112*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"EulerEquations", "[", 
   RowBox[{"Lagrangiana", ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"xp", "[", "t", "]"}], ",", 
      RowBox[{"o", "[", "t", "]"}]}], "}"}], ",", "t"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.8103310801436234`*^9, 3.810331120192163*^9}, {
  3.8103312051625957`*^9, 3.810331225300888*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"L", " ", "M", " ", 
      RowBox[{"Sin", "[", 
       RowBox[{"o", "[", "t", "]"}], "]"}], " ", 
      SuperscriptBox[
       RowBox[{
        SuperscriptBox["o", "\[Prime]",
         MultilineFunction->None], "[", "t", "]"}], "2"]}], "-", 
     RowBox[{"L", " ", "M", " ", 
      RowBox[{"Cos", "[", 
       RowBox[{"o", "[", "t", "]"}], "]"}], " ", 
      RowBox[{
       SuperscriptBox["o", "\[Prime]\[Prime]",
        MultilineFunction->None], "[", "t", "]"}]}], "-", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"m", "+", "M"}], ")"}], " ", 
      RowBox[{
       SuperscriptBox["xp", "\[Prime]\[Prime]",
        MultilineFunction->None], "[", "t", "]"}]}]}], "\[Equal]", "0"}], ",", 
   RowBox[{
    RowBox[{
     RowBox[{"-", "L"}], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"g", " ", "m", " ", 
        RowBox[{"Sin", "[", 
         RowBox[{"o", "[", "t", "]"}], "]"}]}], "+", 
       RowBox[{"L", " ", "M", " ", 
        RowBox[{
         SuperscriptBox["o", "\[Prime]\[Prime]",
          MultilineFunction->None], "[", "t", "]"}]}], "+", 
       RowBox[{"M", " ", 
        RowBox[{"Cos", "[", 
         RowBox[{"o", "[", "t", "]"}], "]"}], " ", 
        RowBox[{
         SuperscriptBox["xp", "\[Prime]\[Prime]",
          MultilineFunction->None], "[", "t", "]"}]}]}], ")"}]}], "\[Equal]", 
    "0"}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.810331124022418*^9, 3.810331206283524*^9, 3.810331337586055*^9, 
   3.810331456053746*^9, {3.810331497843306*^9, 3.810331518944352*^9}, 
   3.8103316622071466`*^9, 3.81033170893014*^9}]
}, Open  ]]
},
WindowSize->{808, 621},
WindowMargins->{{204, Automatic}, {36, Automatic}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 381, 11, 55, "Input"],
Cell[964, 35, 205, 6, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1206, 46, 289, 8, 55, "Input"],
Cell[1498, 56, 288, 7, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1823, 68, 993, 29, 55, "Input"],
Cell[2819, 99, 1104, 34, 49, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3960, 138, 339, 8, 32, InheritFromParent],
Cell[4302, 148, 280, 6, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4619, 159, 246, 5, 32, InheritFromParent],
Cell[4868, 166, 1289, 38, 89, "Output"]
}, Open  ]],
Cell[6172, 207, 186, 3, 55, "Input"],
Cell[CellGroupData[{
Cell[6383, 214, 382, 9, 55, "Input"],
Cell[6768, 225, 1639, 46, 61, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

