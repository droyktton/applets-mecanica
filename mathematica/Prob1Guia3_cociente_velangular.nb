(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     19218,        588]
NotebookOptionsPosition[     18044,        542]
NotebookOutlinePosition[     18377,        557]
CellTagsIndexPosition[     18334,        554]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{"para", " ", "movimiento", " ", "circular"}], ",", " ", 
    RowBox[{
    "para", " ", "un", " ", "dado", " ", "momento", " ", "angular", " ", 
     "p"}], ",", " ", 
    RowBox[{
     RowBox[{"la", " ", "energia", " ", "debe", " ", "ser", " ", "minima"}], 
     " ", "\[Rule]", " ", 
     RowBox[{"la", " ", "derivada", " ", "0"}]}]}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"m", " ", "g", " ", "r", " ", 
       RowBox[{"Cos", "[", "\[Alpha]", "]"}]}], " ", "+", " ", 
      RowBox[{
       RowBox[{"p", "^", "2"}], "/", 
       RowBox[{"(", 
        RowBox[{"2", " ", "m", " ", 
         RowBox[{"r", "^", "2"}], " ", 
         RowBox[{
          RowBox[{"Sin", "[", "\[Alpha]", "]"}], "^", "2"}]}], ")"}]}]}], ",",
      "r"}], "]"}], "\[Equal]", "0"}]}]], "Input",
 CellChangeTimes->{{3.7746193669982357`*^9, 3.774619419351152*^9}, {
  3.774619467082973*^9, 3.774619478511359*^9}, {3.7746197086559553`*^9, 
  3.7746197156718283`*^9}, {3.7749754236022797`*^9, 3.7749754410139217`*^9}, {
  3.774976019426579*^9, 3.7749760531416817`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"g", " ", "m", " ", 
    RowBox[{"Cos", "[", "\[Alpha]", "]"}]}], "-", 
   FractionBox[
    RowBox[{
     SuperscriptBox["p", "2"], " ", 
     SuperscriptBox[
      RowBox[{"Csc", "[", "\[Alpha]", "]"}], "2"]}], 
    RowBox[{"m", " ", 
     SuperscriptBox["r", "3"]}]]}], "\[Equal]", "0"}]], "Output",
 CellChangeTimes->{
  3.774619428964078*^9, {3.774619468271632*^9, 3.774619479310171*^9}, 
   3.774619718187077*^9, 3.774975445554838*^9, {3.7749760295333548`*^9, 
   3.7749760556584797`*^9}, 3.7749764019669867`*^9, 3.774977386399993*^9}]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.774976061038795*^9, 3.7749760800073013`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
   "veamos", " ", "las", " ", "raices", " ", "de", " ", "la", " ", 
    "ecuacion"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{"Solve", "[", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"g", " ", "m", " ", 
       RowBox[{"Cos", "[", "\[Alpha]", "]"}]}], "-", 
      FractionBox[
       RowBox[{
        SuperscriptBox["p", "2"], " ", 
        SuperscriptBox[
         RowBox[{"Csc", "[", "\[Alpha]", "]"}], "2"]}], 
       RowBox[{"m", " ", 
        SuperscriptBox["r", "3"]}]]}], "\[Equal]", "0"}], ",", 
    RowBox[{"{", "r", "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.774976076321589*^9, 3.774976077128415*^9}},
 NumberMarks->False],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"r", "\[Rule]", 
     FractionBox[
      RowBox[{
       SuperscriptBox["p", 
        RowBox[{"2", "/", "3"}]], " ", 
       SuperscriptBox[
        RowBox[{"Csc", "[", "\[Alpha]", "]"}], 
        RowBox[{"2", "/", "3"}]], " ", 
       SuperscriptBox[
        RowBox[{"Sec", "[", "\[Alpha]", "]"}], 
        RowBox[{"1", "/", "3"}]]}], 
      RowBox[{
       SuperscriptBox["g", 
        RowBox[{"1", "/", "3"}]], " ", 
       SuperscriptBox["m", 
        RowBox[{"2", "/", "3"}]]}]]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"r", "\[Rule]", 
     RowBox[{"-", 
      FractionBox[
       RowBox[{
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"-", "1"}], ")"}], 
         RowBox[{"1", "/", "3"}]], " ", 
        SuperscriptBox["p", 
         RowBox[{"2", "/", "3"}]], " ", 
        SuperscriptBox[
         RowBox[{"Csc", "[", "\[Alpha]", "]"}], 
         RowBox[{"2", "/", "3"}]], " ", 
        SuperscriptBox[
         RowBox[{"Sec", "[", "\[Alpha]", "]"}], 
         RowBox[{"1", "/", "3"}]]}], 
       RowBox[{
        SuperscriptBox["g", 
         RowBox[{"1", "/", "3"}]], " ", 
        SuperscriptBox["m", 
         RowBox[{"2", "/", "3"}]]}]]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"r", "\[Rule]", 
     FractionBox[
      RowBox[{
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"-", "1"}], ")"}], 
        RowBox[{"2", "/", "3"}]], " ", 
       SuperscriptBox["p", 
        RowBox[{"2", "/", "3"}]], " ", 
       SuperscriptBox[
        RowBox[{"Csc", "[", "\[Alpha]", "]"}], 
        RowBox[{"2", "/", "3"}]], " ", 
       SuperscriptBox[
        RowBox[{"Sec", "[", "\[Alpha]", "]"}], 
        RowBox[{"1", "/", "3"}]]}], 
      RowBox[{
       SuperscriptBox["g", 
        RowBox[{"1", "/", "3"}]], " ", 
       SuperscriptBox["m", 
        RowBox[{"2", "/", "3"}]]}]]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.7749754511897087`*^9, 3.774976077842353*^9, 
  3.774976404492049*^9, 3.774977390649527*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "el", " ", "minimo", " ", "es", " ", "la", " ", "primera", " ", "raiz", 
    " ", "primero"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"r0", "=", 
    FractionBox[
     RowBox[{
      SuperscriptBox["p", 
       RowBox[{"2", "/", "3"}]], " ", 
      SuperscriptBox[
       RowBox[{"Csc", "[", "\[Alpha]", "]"}], 
       RowBox[{"2", "/", "3"}]], " ", 
      SuperscriptBox[
       RowBox[{"Sec", "[", "\[Alpha]", "]"}], 
       RowBox[{"1", "/", "3"}]]}], 
     RowBox[{
      SuperscriptBox["g", 
       RowBox[{"1", "/", "3"}]], " ", 
      SuperscriptBox["m", 
       RowBox[{"2", "/", "3"}]]}]]}], "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.7749754814853573`*^9, 3.7749755091797457`*^9}, {
  3.7749760879990187`*^9, 3.77497609127567*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox["p", 
    RowBox[{"2", "/", "3"}]], " ", 
   SuperscriptBox[
    RowBox[{"Csc", "[", "\[Alpha]", "]"}], 
    RowBox[{"2", "/", "3"}]], " ", 
   SuperscriptBox[
    RowBox[{"Sec", "[", "\[Alpha]", "]"}], 
    RowBox[{"1", "/", "3"}]]}], 
  RowBox[{
   SuperscriptBox["g", 
    RowBox[{"1", "/", "3"}]], " ", 
   SuperscriptBox["m", 
    RowBox[{"2", "/", "3"}]]}]]], "Output",
 CellChangeTimes->{3.774976407498424*^9, 3.77497739410455*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
   "desarrollamos", " ", "el", " ", "potencial", " ", "alrededor", " ", "de", 
    " ", "r0", " ", "a", " ", "orden", " ", "2"}], " ", "*)"}], 
  "\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{"Series", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"m", " ", "g", " ", "r", " ", 
      RowBox[{"Cos", "[", "\[Alpha]", "]"}]}], " ", "+", " ", 
     RowBox[{
      RowBox[{"p", "^", "2"}], "/", 
      RowBox[{"(", 
       RowBox[{"2", " ", "m", " ", 
        RowBox[{"r", "^", "2"}], " ", 
        RowBox[{
         RowBox[{"Sin", "[", "\[Alpha]", "]"}], "^", "2"}]}], ")"}]}]}], ",", 
    
    RowBox[{"{", 
     RowBox[{"r", ",", "r0", ",", "2"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.774619501090666*^9, 3.774619519063653*^9}, {
  3.7746195595997562`*^9, 3.77461958939925*^9}, {3.7746197385868053`*^9, 
  3.774619751584138*^9}, {3.774975498098378*^9, 3.774975507734696*^9}, {
  3.774975550938325*^9, 3.774975579737425*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
   FractionBox[
    RowBox[{"3", " ", 
     SuperscriptBox["g", 
      RowBox[{"2", "/", "3"}]], " ", 
     SuperscriptBox["m", 
      RowBox[{"1", "/", "3"}]], " ", 
     SuperscriptBox["p", 
      RowBox[{"2", "/", "3"}]], " ", 
     SuperscriptBox[
      RowBox[{"Csc", "[", "\[Alpha]", "]"}], 
      RowBox[{"2", "/", "3"}]]}], 
    RowBox[{"2", " ", 
     SuperscriptBox[
      RowBox[{"Sec", "[", "\[Alpha]", "]"}], 
      RowBox[{"2", "/", "3"}]]}]], "+", 
   FractionBox[
    RowBox[{"3", " ", 
     SuperscriptBox["g", 
      RowBox[{"4", "/", "3"}]], " ", 
     SuperscriptBox["m", 
      RowBox[{"5", "/", "3"}]], " ", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"r", "-", 
        FractionBox[
         RowBox[{
          SuperscriptBox["p", 
           RowBox[{"2", "/", "3"}]], " ", 
          SuperscriptBox[
           RowBox[{"Csc", "[", "\[Alpha]", "]"}], 
           RowBox[{"2", "/", "3"}]], " ", 
          SuperscriptBox[
           RowBox[{"Sec", "[", "\[Alpha]", "]"}], 
           RowBox[{"1", "/", "3"}]]}], 
         RowBox[{
          SuperscriptBox["g", 
           RowBox[{"1", "/", "3"}]], " ", 
          SuperscriptBox["m", 
           RowBox[{"2", "/", "3"}]]}]]}], ")"}], "2"]}], 
    RowBox[{"2", " ", 
     SuperscriptBox["p", 
      RowBox[{"2", "/", "3"}]], " ", 
     SuperscriptBox[
      RowBox[{"Csc", "[", "\[Alpha]", "]"}], 
      RowBox[{"2", "/", "3"}]], " ", 
     SuperscriptBox[
      RowBox[{"Sec", "[", "\[Alpha]", "]"}], 
      RowBox[{"4", "/", "3"}]]}]], "+", 
   InterpretationBox[
    SuperscriptBox[
     RowBox[{"O", "[", 
      RowBox[{"r", "-", 
       FractionBox[
        RowBox[{
         SuperscriptBox["p", 
          RowBox[{"2", "/", "3"}]], " ", 
         SuperscriptBox[
          RowBox[{"Csc", "[", "\[Alpha]", "]"}], 
          RowBox[{"2", "/", "3"}]], " ", 
         SuperscriptBox[
          RowBox[{"Sec", "[", "\[Alpha]", "]"}], 
          RowBox[{"1", "/", "3"}]]}], 
        RowBox[{
         SuperscriptBox["g", 
          RowBox[{"1", "/", "3"}]], " ", 
         SuperscriptBox["m", 
          RowBox[{"2", "/", "3"}]]}]]}], "]"}], "3"],
    SeriesData[$CellContext`r, $CellContext`g^Rational[-1, 3] $CellContext`m^
      Rational[-2, 3] $CellContext`p^Rational[2, 3] 
     Csc[$CellContext`\[Alpha]]^Rational[2, 3] 
     Sec[$CellContext`\[Alpha]]^Rational[1, 3], {}, 0, 3, 1],
    Editable->False]}],
  SeriesData[$CellContext`r, $CellContext`g^Rational[-1, 3] $CellContext`m^
    Rational[-2, 3] $CellContext`p^Rational[2, 3] 
   Csc[$CellContext`\[Alpha]]^Rational[2, 3] 
   Sec[$CellContext`\[Alpha]]^Rational[1, 3], {
   Rational[3, 2] $CellContext`g^Rational[2, 3] $CellContext`m^
     Rational[1, 3] $CellContext`p^Rational[2, 3] 
    Csc[$CellContext`\[Alpha]]^Rational[2, 3] 
    Sec[$CellContext`\[Alpha]]^Rational[-2, 3], 0, 
    Rational[3, 2] $CellContext`g^Rational[4, 3] $CellContext`m^
     Rational[5, 3] $CellContext`p^Rational[-2, 3] 
    Csc[$CellContext`\[Alpha]]^Rational[-2, 3] 
    Sec[$CellContext`\[Alpha]]^Rational[-4, 3]}, 0, 3, 1],
  Editable->False]], "Output",
 CellChangeTimes->{
  3.774619600116129*^9, {3.774619739783383*^9, 3.774619753628992*^9}, 
   3.774953993785172*^9, 3.77495407138666*^9, {3.774975491077612*^9, 
   3.774975503726809*^9}, 3.774975583747959*^9, 3.774976117488617*^9, 
   3.774976410754108*^9, 3.7749773982715473`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
    "la", " ", "constante", " ", "de", " ", "resorte", " ", "kappa", " ", 
     "es", " ", "el", " ", "coeficiente", " ", "cuadratico", " ", "por", " ", 
     "2"}], ",", " ", 
    RowBox[{
     RowBox[{"i", ".", "e", ".", " ", "U"}], "=", 
     RowBox[{
      RowBox[{"U", 
       RowBox[{"(", "r0", ")"}]}], "+", 
      RowBox[{
       RowBox[{"U", "''"}], 
       RowBox[{"(", "r0", ")"}], " ", 
       RowBox[{
        RowBox[{
         RowBox[{"(", 
          RowBox[{"r", "-", "r0"}], ")"}], "^", "2"}], "/", "2"}]}]}]}], ",", 
    " ", "entonces"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{"kappa", "=", 
   FractionBox[
    RowBox[{"3", " ", 
     SuperscriptBox["g", 
      RowBox[{"4", "/", "3"}]], " ", 
     SuperscriptBox["m", 
      RowBox[{"5", "/", "3"}]], " "}], 
    RowBox[{
     SuperscriptBox["p", 
      RowBox[{"2", "/", "3"}]], " ", 
     SuperscriptBox[
      RowBox[{"Csc", "[", "\[Alpha]", "]"}], 
      RowBox[{"2", "/", "3"}]], " ", 
     SuperscriptBox[
      RowBox[{"Sec", "[", "\[Alpha]", "]"}], 
      RowBox[{"4", "/", "3"}]]}]]}]}]], "Input",
 CellChangeTimes->{{3.774619569637863*^9, 3.77461956965256*^9}, {
  3.7746196465512857`*^9, 3.774619665967169*^9}, {3.77461976812748*^9, 
  3.7746197745591173`*^9}, {3.77495393365388*^9, 3.774953940925324*^9}, {
  3.774954002068459*^9, 3.774954003040565*^9}, {3.774954102909379*^9, 
  3.774954103461688*^9}, {3.774975620791848*^9, 3.774975629469831*^9}, {
  3.77497612847609*^9, 3.774976176143403*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"3", " ", 
   SuperscriptBox["g", 
    RowBox[{"4", "/", "3"}]], " ", 
   SuperscriptBox["m", 
    RowBox[{"5", "/", "3"}]]}], 
  RowBox[{
   SuperscriptBox["p", 
    RowBox[{"2", "/", "3"}]], " ", 
   SuperscriptBox[
    RowBox[{"Csc", "[", "\[Alpha]", "]"}], 
    RowBox[{"2", "/", "3"}]], " ", 
   SuperscriptBox[
    RowBox[{"Sec", "[", "\[Alpha]", "]"}], 
    RowBox[{"4", "/", "3"}]]}]]], "Output",
 CellChangeTimes->{
  3.774954003771605*^9, 3.774954104491869*^9, 3.77497563817407*^9, 
   3.774976178019279*^9, 3.774976415862389*^9, {3.774977413239526*^9, 
   3.774977417688521*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{"omega_r", " ", "=", " ", 
     RowBox[{
      RowBox[{"Sqrt", 
       RowBox[{"(", 
        RowBox[{"kappa", "/", "m"}], ")"}], " ", "mientras", " ", "que", " ", 
       "omega_phi"}], " ", "=", " ", 
      RowBox[{"phidot", " ", "=", " ", 
       RowBox[{
        RowBox[{"p", "/", "m"}], " ", 
        RowBox[{
         RowBox[{"Sin", "[", "\[Alpha]", "]"}], "^", "2"}], " ", 
        RowBox[{"r0", "^", "2"}]}]}]}]}], ",", " ", 
    RowBox[{
     RowBox[{"usando", " ", "que", " ", "p"}], " ", "=", " ", 
     RowBox[{"m", " ", 
      RowBox[{"r0", "^", "2"}], " ", "phidot"}]}]}], " ", "*)"}], 
  "\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"omegar2", " ", "=", " ", 
    RowBox[{"(", 
     RowBox[{"kappa", "/", "m"}], ")"}]}], "\[IndentingNewLine]"}]}]], "Input",\

 CellChangeTimes->{{3.7746197705977182`*^9, 3.774619770614295*^9}, {
   3.774953901198491*^9, 3.7749540148489227`*^9}, {3.7749541436661987`*^9, 
   3.774954146768426*^9}, {3.7749552348684673`*^9, 3.774955239562295*^9}, {
   3.774975654860877*^9, 3.774975704715826*^9}, {3.7749758982613297`*^9, 
   3.774975904709194*^9}, {3.7749761983091993`*^9, 3.774976256474522*^9}, {
   3.774976315891839*^9, 3.774976345119952*^9}, {3.77497637638477*^9, 
   3.774976376562956*^9}, {3.774976517410451*^9, 3.774976524664485*^9}, 
   3.7749765939812813`*^9, {3.7749774346028757`*^9, 3.774977438662232*^9}, {
   3.774988666576165*^9, 3.7749886808897333`*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"3", " ", 
   SuperscriptBox["g", 
    RowBox[{"4", "/", "3"}]], " ", 
   SuperscriptBox["m", 
    RowBox[{"2", "/", "3"}]]}], 
  RowBox[{
   SuperscriptBox["p", 
    RowBox[{"2", "/", "3"}]], " ", 
   SuperscriptBox[
    RowBox[{"Csc", "[", "\[Alpha]", "]"}], 
    RowBox[{"2", "/", "3"}]], " ", 
   SuperscriptBox[
    RowBox[{"Sec", "[", "\[Alpha]", "]"}], 
    RowBox[{"4", "/", "3"}]]}]]], "Output",
 CellChangeTimes->{
  3.7749756468264627`*^9, 3.774975746582847*^9, 3.77497590530896*^9, {
   3.774976353739915*^9, 3.774976381001524*^9}, 3.774976419647111*^9, 
   3.77497652526222*^9, 3.774976601182415*^9, {3.774977423442573*^9, 
   3.774977439596417*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"omegaphi2", " ", "=", " ", 
   RowBox[{
    RowBox[{"p", "^", "2"}], "/", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"m", " ", 
       RowBox[{"r0", "^", "2"}], " ", 
       RowBox[{
        RowBox[{"Sin", "[", "\[Phi]", "]"}], "^", "2"}]}], ")"}], "^", 
     "2"}]}]}]}]], "Input",
 CellChangeTimes->{{3.774976543028974*^9, 3.774976564589532*^9}, 
   3.774976598442547*^9, {3.7749774526737537`*^9, 3.774977457405765*^9}, {
   3.774988708745502*^9, 3.774988717162561*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox["g", 
    RowBox[{"4", "/", "3"}]], " ", 
   SuperscriptBox["m", 
    RowBox[{"2", "/", "3"}]], " ", 
   SuperscriptBox[
    RowBox[{"Csc", "[", "\[Phi]", "]"}], "4"]}], 
  RowBox[{
   SuperscriptBox["p", 
    RowBox[{"2", "/", "3"}]], " ", 
   SuperscriptBox[
    RowBox[{"Csc", "[", "\[Alpha]", "]"}], 
    RowBox[{"8", "/", "3"}]], " ", 
   SuperscriptBox[
    RowBox[{"Sec", "[", "\[Alpha]", "]"}], 
    RowBox[{"4", "/", "3"}]]}]]], "Output",
 CellChangeTimes->{{3.7749765590783663`*^9, 3.7749765655855427`*^9}, 
   3.774976603299404*^9, 3.774977457890047*^9, 3.774988719337727*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"omegar2", "/", "omegaphi2"}]}]], "Input",
 CellChangeTimes->{{3.7749765684128637`*^9, 3.774976608681529*^9}, {
  3.774977465581411*^9, 3.7749774673372993`*^9}}],

Cell[BoxData[
 RowBox[{"3", " ", 
  SuperscriptBox[
   RowBox[{"Csc", "[", "\[Alpha]", "]"}], "2"], " ", 
  SuperscriptBox[
   RowBox[{"Sin", "[", "\[Phi]", "]"}], "4"]}]], "Output",
 CellChangeTimes->{{3.774976578672619*^9, 3.774976609175783*^9}, 
   3.774977467721251*^9, 3.7749887217762403`*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{
     RowBox[{"O", " ", "sea", " ", "que", " ", 
      RowBox[{"omegaphi", "/", "omegar"}]}], " ", "=", " ", 
     RowBox[{
      RowBox[{"Sin", "[", "\[Alpha]", "]"}], "/", 
      RowBox[{"Sqrt", "[", "3", "]"}]}]}], ",", " ", 
    RowBox[{
     RowBox[{
      RowBox[{"distinto", " ", "a", " ", "lo", " ", "de", " ", "Franco"}], 
      "..."}], " ", "donde", " ", "esta", " ", "el", " ", 
     RowBox[{"error", "?"}]}]}], " ", "*)"}]}]], "Input",
 CellChangeTimes->{{3.774977505753516*^9, 3.77497758024022*^9}}]
},
WindowSize->{1301, 744},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 1208, 30, 77, "Input"],
Cell[1791, 54, 586, 15, 53, "Output"]
}, Open  ]],
Cell[2392, 72, 94, 1, 32, "Input"],
Cell[CellGroupData[{
Cell[2511, 77, 716, 21, 84, "Input"],
Cell[3230, 100, 2037, 64, 55, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5304, 169, 861, 24, 135, "Input"],
Cell[6168, 195, 495, 16, 55, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6700, 216, 1050, 25, 121, "Input"],
Cell[7753, 243, 3414, 92, 69, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11204, 340, 1559, 42, 90, "Input"],
Cell[12766, 384, 628, 19, 57, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13431, 408, 1510, 34, 99, "Input"],
Cell[14944, 444, 701, 20, 57, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15682, 469, 534, 14, 55, "Input"],
Cell[16219, 485, 644, 19, 56, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[16900, 509, 218, 4, 55, "Input"],
Cell[17121, 515, 298, 7, 67, "Output"]
}, Open  ]],
Cell[17434, 525, 606, 15, 55, "Input"]
}
]
*)

(* End of internal cache information *)
