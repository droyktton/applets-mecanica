(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[         0,          0]
NotebookDataLength[      7622,        243]
NotebookOptionsPosition[      6863,        211]
NotebookOutlinePosition[      7196,        226]
CellTagsIndexPosition[      7153,        223]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"x1", "[", 
   RowBox[{"t_", ",", " ", "omega_"}], "]"}], ":=", 
  RowBox[{"omega", "*", 
   RowBox[{"Sin", "[", 
    RowBox[{"omega", " ", "t"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.773504536737173*^9, 3.7735046756562147`*^9}}],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"y1", "[", 
     RowBox[{"t_", ",", " ", "omega_"}], "]"}], ":=", 
    RowBox[{"omega", "*", 
     RowBox[{"Cos", "[", 
      RowBox[{"omega", " ", "t"}], "]"}]}]}], 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.77350465430593*^9, 3.7735046610951*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"dotx1", "[", 
   RowBox[{"t_", ",", " ", "omega_"}], "]"}], ":=", 
  RowBox[{"D", "[", 
   RowBox[{
    RowBox[{"x1", "[", 
     RowBox[{"t", ",", "omega"}], "]"}], ",", "t"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.77350470593408*^9, 3.773504738544366*^9}}],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"doty1", "[", 
    RowBox[{"t_", ",", " ", "omega_"}], "]"}], ":=", 
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{"y1", "[", 
      RowBox[{"t", ",", "omega"}], "]"}], ",", "t"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.773504743140234*^9, 3.773504748582963*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"Lagrangiana", "=", 
    RowBox[{
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"dotx1", "[", 
          RowBox[{"t", ",", "omega"}], "]"}], "^", "2"}], "+", 
        RowBox[{
         RowBox[{"doty1", "[", 
          RowBox[{"t", ",", "omega"}], "]"}], "^", "2"}]}], ")"}], "/", "2"}],
      "+", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"dotx1", "[", 
            RowBox[{"t", ",", "omega"}], "]"}], "+", 
           RowBox[{
            RowBox[{
             RowBox[{"theta", "'"}], "[", "t", "]"}], "*", 
            RowBox[{"Cos", "[", 
             RowBox[{"theta", "[", "t", "]"}], "]"}]}]}], ")"}], "^", "2"}], 
        "+", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"doty1", "[", 
            RowBox[{"t", ",", "omega"}], "]"}], "-", 
           RowBox[{
            RowBox[{
             RowBox[{"theta", "'"}], "[", "t", "]"}], "*", 
            RowBox[{"Sin", "[", 
             RowBox[{"theta", "[", "t", "]"}], "]"}]}]}], ")"}], "^", "2"}]}],
        ")"}], "/", "2"}], "+", 
     RowBox[{"g", "*", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"2", "*", 
         RowBox[{"y1", "[", 
          RowBox[{"t", ",", "omega"}], "]"}]}], "+", 
        RowBox[{"Cos", "[", 
         RowBox[{"theta", "[", "t", "]"}], "]"}]}], ")"}]}]}]}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.7735047555553923`*^9, 3.7735049468268833`*^9}, {
  3.773505006467144*^9, 3.773505006503915*^9}, {3.773505940428473*^9, 
  3.773505941594693*^9}, {3.773506016337475*^9, 3.773506069611347*^9}, {
  3.773506105228484*^9, 3.773506109360755*^9}, {3.773506154849328*^9, 
  3.773506155144904*^9}, {3.7735078978918543`*^9, 3.7735079241892147`*^9}, {
  3.773508365668091*^9, 3.773508366139518*^9}, {3.7735211252808113`*^9, 
  3.7735211275617123`*^9}, {3.7735216113715563`*^9, 3.77352161200848*^9}, {
  3.773522313049917*^9, 3.773522335897735*^9}, {3.773522371789996*^9, 
  3.773522399786747*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"g", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"2", " ", "omega", " ", 
      RowBox[{"Cos", "[", 
       RowBox[{"omega", " ", "t"}], "]"}]}], "+", 
     RowBox[{"Cos", "[", 
      RowBox[{"theta", "[", "t", "]"}], "]"}]}], ")"}]}], "+", 
  RowBox[{
   FractionBox["1", "2"], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      SuperscriptBox["omega", "4"], " ", 
      SuperscriptBox[
       RowBox[{"Cos", "[", 
        RowBox[{"omega", " ", "t"}], "]"}], "2"]}], "+", 
     RowBox[{
      SuperscriptBox["omega", "4"], " ", 
      SuperscriptBox[
       RowBox[{"Sin", "[", 
        RowBox[{"omega", " ", "t"}], "]"}], "2"]}]}], ")"}]}], "+", 
  RowBox[{
   FractionBox["1", "2"], " ", 
   RowBox[{"(", 
    RowBox[{
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         SuperscriptBox["omega", "2"], " ", 
         RowBox[{"Cos", "[", 
          RowBox[{"omega", " ", "t"}], "]"}]}], "+", 
        RowBox[{
         RowBox[{"Cos", "[", 
          RowBox[{"theta", "[", "t", "]"}], "]"}], " ", 
         RowBox[{
          SuperscriptBox["theta", "\[Prime]",
           MultilineFunction->None], "[", "t", "]"}]}]}], ")"}], "2"], "+", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"-", 
          SuperscriptBox["omega", "2"]}], " ", 
         RowBox[{"Sin", "[", 
          RowBox[{"omega", " ", "t"}], "]"}]}], "-", 
        RowBox[{
         RowBox[{"Sin", "[", 
          RowBox[{"theta", "[", "t", "]"}], "]"}], " ", 
         RowBox[{
          SuperscriptBox["theta", "\[Prime]",
           MultilineFunction->None], "[", "t", "]"}]}]}], ")"}], "2"]}], 
    ")"}]}]}]], "Output",
 CellChangeTimes->{3.773522400459453*^9, 3.773522439742853*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"Needs", "[", "\"\<VariationalMethods`\>\"", "]"}]], "Input"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"EulerEquations", "[", 
   RowBox[{"Lagrangiana", ",", 
    RowBox[{"theta", "[", "t", "]"}], ",", "t"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.773506121199438*^9, 3.7735061615010138`*^9}, {
   3.773507766555954*^9, 3.7735077934384403`*^9}, {3.773507833778935*^9, 
   3.77350783494738*^9}, {3.773507910487769*^9, 3.773507938069213*^9}, 
   3.773508378557724*^9, 3.773521141690198*^9, 3.773521393866667*^9, {
   3.773522325808757*^9, 3.773522327412434*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    SuperscriptBox["omega", "3"], " ", 
    RowBox[{"Sin", "[", 
     RowBox[{
      RowBox[{"omega", " ", "t"}], "-", 
      RowBox[{"theta", "[", "t", "]"}]}], "]"}]}], "-", 
   RowBox[{"g", " ", 
    RowBox[{"Sin", "[", 
     RowBox[{"theta", "[", "t", "]"}], "]"}]}], "-", 
   RowBox[{
    SuperscriptBox["theta", "\[Prime]\[Prime]",
     MultilineFunction->None], "[", "t", "]"}]}], "\[Equal]", "0"}]], "Output",\

 CellChangeTimes->{{3.773522340100604*^9, 3.7735223757513733`*^9}, 
   3.773522407854602*^9, 3.773522442150366*^9}]
}, Open  ]]
},
WindowSize->{1301, 744},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[400, 13, 269, 7, 32, "Input"],
Cell[672, 22, 351, 10, 77, "Input"],
Cell[1026, 34, 295, 8, 32, "Input"],
Cell[1324, 44, 338, 9, 55, "Input"],
Cell[CellGroupData[{
Cell[1687, 57, 2158, 57, 143, "Input"],
Cell[3848, 116, 1774, 56, 89, "Output"]
}, Open  ]],
Cell[5637, 175, 84, 1, 32, "Input"],
Cell[CellGroupData[{
Cell[5746, 180, 516, 9, 55, "Input"],
Cell[6265, 191, 582, 17, 34, "Output"]
}, Open  ]]
}
]
*)

