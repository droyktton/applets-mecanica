(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      5999,        210]
NotebookOptionsPosition[      5341,        183]
NotebookOutlinePosition[      5674,        198]
CellTagsIndexPosition[      5631,        195]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"Integrate", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"(", 
     RowBox[{"1", "/", 
      RowBox[{"r", "^", "2"}]}], ")"}], "/", 
    RowBox[{"Sqrt", "[", 
     RowBox[{"1", "-", 
      RowBox[{"A", "/", 
       RowBox[{"r", "^", "2"}]}]}], "]"}]}], ",", 
   RowBox[{"{", 
    RowBox[{"r", ",", 
     RowBox[{"Sqrt", "[", "A", "]"}], ",", "Infinity"}], "}"}], ",", 
   RowBox[{"Assumptions", "\[Rule]", 
    RowBox[{"A", ">", "0"}]}]}], "]"}]], "Input",
 CellChangeTimes->{{3.776590089780635*^9, 3.776590179257312*^9}, {
  3.776590214202413*^9, 3.77659022183895*^9}, {3.776590349613285*^9, 
  3.776590408105852*^9}, {3.776590465965569*^9, 3.776590486302199*^9}}],

Cell[BoxData[
 FractionBox["\[Pi]", 
  RowBox[{"2", " ", 
   SqrtBox["A"]}]]], "Output",
 CellChangeTimes->{
  3.7765901862819366`*^9, 3.776590235162883*^9, {3.77659037040618*^9, 
   3.77659040886166*^9}, 3.776590439097139*^9, {3.776590469250513*^9, 
   3.7765904886772633`*^9}, {3.77659165781139*^9, 3.776591687289123*^9}}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{"(*", " ", 
   RowBox[{"tenemos", " ", "que", " ", "resolver"}], " ", "*)"}], 
  "\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"\[Chi]", "-", 
    RowBox[{"\[Pi]", "*", 
     RowBox[{"(", 
      RowBox[{"1", "-", 
       RowBox[{"1", "/", 
        RowBox[{"Sqrt", "[", 
         RowBox[{"1", "+", 
          RowBox[{"B", "/", 
           RowBox[{"\[Rho]", "^", "2"}]}]}], "]"}]}]}], ")"}]}]}], "\[Equal]",
    "0"}]}]], "Input",
 CellChangeTimes->{{3.7765924072529297`*^9, 3.77659247435288*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Solve", "[", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"-", "\[Pi]"}], " ", 
      RowBox[{"(", 
       RowBox[{"1", "-", 
        FractionBox["1", 
         SqrtBox[
          RowBox[{"1", "+", 
           FractionBox["B", 
            SuperscriptBox["\[Rho]", "2"]]}]]]}], ")"}]}], "+", "\[Chi]"}], 
    "\[Equal]", "0"}], ",", "\[Rho]"}], "]"}]], "Input",
 CellChangeTimes->{{3.776592477462681*^9, 3.7765925603444853`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"\[Rho]", "\[Rule]", 
     RowBox[{"-", 
      FractionBox[
       RowBox[{"\[ImaginaryI]", " ", 
        SqrtBox["B"], " ", 
        RowBox[{"(", 
         RowBox[{"\[Pi]", "-", "\[Chi]"}], ")"}]}], 
       RowBox[{
        SqrtBox["\[Chi]"], " ", 
        SqrtBox[
         RowBox[{
          RowBox[{
           RowBox[{"-", "2"}], " ", "\[Pi]"}], "+", "\[Chi]"}]]}]]}]}], "}"}],
    ",", 
   RowBox[{"{", 
    RowBox[{"\[Rho]", "\[Rule]", 
     FractionBox[
      RowBox[{"\[ImaginaryI]", " ", 
       SqrtBox["B"], " ", 
       RowBox[{"(", 
        RowBox[{"\[Pi]", "-", "\[Chi]"}], ")"}]}], 
      RowBox[{
       SqrtBox["\[Chi]"], " ", 
       SqrtBox[
        RowBox[{
         RowBox[{
          RowBox[{"-", "2"}], " ", "\[Pi]"}], "+", "\[Chi]"}]]}]]}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{
  3.776592488091689*^9, {3.7765925379337482`*^9, 3.776592561074576*^9}}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"solucion", " ", "usando", " ", "que", " ", "\[Chi]"}], "<", 
    RowBox[{"2", "\[Pi]"}]}], "*)"}], "\[IndentingNewLine]", 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"\[Rho]", "[", "\[Chi]_", "]"}], ":=", 
   FractionBox[
    RowBox[{
     SqrtBox["B"], " ", 
     RowBox[{"(", 
      RowBox[{"\[Pi]", "-", "\[Chi]"}], ")"}]}], 
    RowBox[{
     SqrtBox["\[Chi]"], " ", 
     SqrtBox[
      RowBox[{
       RowBox[{"-", "\[Chi]"}], "+", 
       RowBox[{"2", " ", "\[Pi]"}]}]]}]]}]}]], "Input",
 CellChangeTimes->{{3.776592499409532*^9, 3.776592510676414*^9}, {
  3.776592564380546*^9, 3.7765926163627367`*^9}, {3.776592655648369*^9, 
  3.776592669422954*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Simplify", "[", 
  RowBox[{
   RowBox[{"\[Rho]", "[", "\[Chi]", "]"}], 
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{"\[Rho]", "[", "\[Chi]", "]"}], ",", "\[Chi]"}], "]"}]}], "]"}]],\
 "Input",
 CellChangeTimes->{{3.776592629532907*^9, 3.776592637742037*^9}, {
  3.776592678446154*^9, 3.776592715480637*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"B", " ", 
    SuperscriptBox["\[Pi]", "2"], " ", 
    RowBox[{"(", 
     RowBox[{"\[Pi]", "-", "\[Chi]"}], ")"}]}], 
   RowBox[{
    SuperscriptBox["\[Chi]", "2"], " ", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       RowBox[{
        RowBox[{"-", "2"}], " ", "\[Pi]"}], "+", "\[Chi]"}], ")"}], 
     "2"]}]]}]], "Output",
 CellChangeTimes->{3.776592716077499*^9}]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.776592706719139*^9, 3.7765927088126163`*^9}}]
},
WindowSize->{1301, 744},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 685, 18, 32, "Input"],
Cell[1268, 42, 324, 7, 51, "Output"]
}, Open  ]],
Cell[1607, 52, 598, 16, 121, "Input"],
Cell[CellGroupData[{
Cell[2230, 72, 473, 15, 115, InheritFromParent],
Cell[2706, 89, 957, 33, 60, "Output"]
}, Open  ]],
Cell[3678, 125, 754, 22, 141, "Input"],
Cell[CellGroupData[{
Cell[4457, 151, 335, 9, 32, InheritFromParent],
Cell[4795, 162, 433, 15, 56, "Output"]
}, Open  ]],
Cell[5243, 180, 94, 1, 32, "Input"]
}
]
*)

(* End of internal cache information *)
