(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      5355,        187]
NotebookOptionsPosition[      4832,        163]
NotebookOutlinePosition[      5165,        178]
CellTagsIndexPosition[      5122,        175]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"Lagrangiano", " ", "=", " ", 
   RowBox[{
    RowBox[{"Sqrt", "[", 
     RowBox[{"1", "+", 
      RowBox[{
       RowBox[{
        RowBox[{"y", "'"}], "[", "x", "]"}], "^", "2"}]}], "]"}], "/", 
    RowBox[{"y", "[", "x", "]"}]}]}]}]], "Input",
 CellChangeTimes->{{3.77438405289498*^9, 3.774384071258429*^9}, {
  3.77438466556493*^9, 3.774384677387599*^9}, {3.774384733331601*^9, 
  3.774384734662706*^9}}],

Cell[BoxData[
 FractionBox[
  SqrtBox[
   RowBox[{"1", "+", 
    SuperscriptBox[
     RowBox[{
      SuperscriptBox["y", "\[Prime]",
       MultilineFunction->None], "[", "x", "]"}], "2"]}]], 
  RowBox[{"y", "[", "x", "]"}]]], "Output",
 CellChangeTimes->{3.774384072365883*^9, 3.77438455642494*^9, 
  3.7743846783555193`*^9, 3.77438473540587*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"Needs", "[", "\"\<VariationalMethods`\>\"", "]"}], 
   "\[IndentingNewLine]", 
   RowBox[{"EulerEquations", "[", 
    RowBox[{"Lagrangiano", ",", 
     RowBox[{"{", 
      RowBox[{"y", "[", "x", "]"}], "}"}], ",", "x"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.774384085365748*^9, 3.7743840967160807`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox[
    RowBox[{
     RowBox[{"-", "1"}], "-", 
     SuperscriptBox[
      RowBox[{
       SuperscriptBox["y", "\[Prime]",
        MultilineFunction->None], "[", "x", "]"}], "2"], "-", 
     RowBox[{
      RowBox[{"y", "[", "x", "]"}], " ", 
      RowBox[{
       SuperscriptBox["y", "\[Prime]\[Prime]",
        MultilineFunction->None], "[", "x", "]"}]}]}], 
    RowBox[{
     SuperscriptBox[
      RowBox[{"y", "[", "x", "]"}], "2"], " ", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"1", "+", 
        SuperscriptBox[
         RowBox[{
          SuperscriptBox["y", "\[Prime]",
           MultilineFunction->None], "[", "x", "]"}], "2"]}], ")"}], 
      RowBox[{"3", "/", "2"}]]}]], "\[Equal]", "0"}], "}"}]], "Output",
 CellChangeTimes->{
  3.774384098740327*^9, 3.774384558725142*^9, {3.77438468034046*^9, 
   3.774384684153216*^9}, 3.774384737265285*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"DSolve", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     FractionBox[
      RowBox[{
       RowBox[{"-", "1"}], "-", 
       SuperscriptBox[
        RowBox[{
         SuperscriptBox["y", "\[Prime]",
          MultilineFunction->None], "[", "x", "]"}], "2"], "-", 
       RowBox[{
        RowBox[{"y", "[", "x", "]"}], " ", 
        RowBox[{
         SuperscriptBox["y", "\[Prime]\[Prime]",
          MultilineFunction->None], "[", "x", "]"}]}]}], 
      RowBox[{
       SuperscriptBox[
        RowBox[{"y", "[", "x", "]"}], "2"], " ", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"1", "+", 
          SuperscriptBox[
           RowBox[{
            SuperscriptBox["y", "\[Prime]",
             MultilineFunction->None], "[", "x", "]"}], "2"]}], ")"}], 
        RowBox[{"3", "/", "2"}]]}]], "\[Equal]", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"y", "[", "x", "]"}], ",", 
     RowBox[{"y", "[", "x", "]"}]}], "}"}], ",", 
   RowBox[{"{", "x", "}"}]}], "]"}]], "Input",
 NumberMarks->False],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"y", "[", "x", "]"}], "\[Rule]", 
     RowBox[{"-", 
      SqrtBox[
       RowBox[{
        SuperscriptBox["\[ExponentialE]", 
         RowBox[{"2", " ", 
          RowBox[{"C", "[", "1", "]"}]}]], "-", 
        SuperscriptBox["x", "2"], "-", 
        RowBox[{"2", " ", "x", " ", 
         RowBox[{"C", "[", "2", "]"}]}], "-", 
        SuperscriptBox[
         RowBox[{"C", "[", "2", "]"}], "2"]}]]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"y", "[", "x", "]"}], "\[Rule]", 
     SqrtBox[
      RowBox[{
       SuperscriptBox["\[ExponentialE]", 
        RowBox[{"2", " ", 
         RowBox[{"C", "[", "1", "]"}]}]], "-", 
       SuperscriptBox["x", "2"], "-", 
       RowBox[{"2", " ", "x", " ", 
        RowBox[{"C", "[", "2", "]"}]}], "-", 
       SuperscriptBox[
        RowBox[{"C", "[", "2", "]"}], "2"]}]]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.77438499656378*^9}]
}, Open  ]]
},
WindowSize->{1301, 744},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 464, 12, 55, "Input"],
Cell[1047, 36, 347, 10, 63, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1431, 51, 379, 9, 77, "Input"],
Cell[1813, 62, 924, 28, 59, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2774, 95, 1056, 33, 67, "Input"],
Cell[3833, 130, 983, 30, 104, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

