(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     20496,        651]
NotebookOptionsPosition[     18965,        597]
NotebookOutlinePosition[     19300,        612]
CellTagsIndexPosition[     19257,        609]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"MM", "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        RowBox[{"2", " ", "m", " ", 
         RowBox[{"R", "^", "2"}]}], ",", " ", 
        RowBox[{
         RowBox[{"-", "m"}], " ", "R", "  ", "b", " ", 
         RowBox[{"Cos", "[", 
          RowBox[{"\[Alpha]", "/", "2"}], "]"}]}]}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{
         RowBox[{"-", "m"}], " ", "R", "  ", "b", " ", 
         RowBox[{"Cos", "[", 
          RowBox[{"\[Alpha]", "/", "2"}], "]"}]}], ",", 
        RowBox[{"m", " ", 
         RowBox[{"b", "^", "2"}]}]}], "}"}]}], "}"}]}], "\[IndentingNewLine]", 
   RowBox[{"MatrixForm", "[", "MM", "]"}]}]}]], "Input",
 CellChangeTimes->{{3.777490946447914*^9, 3.7774909541832333`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"2", " ", "m", " ", 
      SuperscriptBox["R", "2"]}], ",", 
     RowBox[{
      RowBox[{"-", "b"}], " ", "m", " ", "R", " ", 
      RowBox[{"Cos", "[", 
       FractionBox["\[Alpha]", "2"], "]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "b"}], " ", "m", " ", "R", " ", 
      RowBox[{"Cos", "[", 
       FractionBox["\[Alpha]", "2"], "]"}]}], ",", 
     RowBox[{
      SuperscriptBox["b", "2"], " ", "m"}]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.7774909547875643`*^9}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      RowBox[{"2", " ", "m", " ", 
       SuperscriptBox["R", "2"]}], 
      RowBox[{
       RowBox[{"-", "b"}], " ", "m", " ", "R", " ", 
       RowBox[{"Cos", "[", 
        FractionBox["\[Alpha]", "2"], "]"}]}]},
     {
      RowBox[{
       RowBox[{"-", "b"}], " ", "m", " ", "R", " ", 
       RowBox[{"Cos", "[", 
        FractionBox["\[Alpha]", "2"], "]"}]}], 
      RowBox[{
       SuperscriptBox["b", "2"], " ", "m"}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.777490954788844*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"KK", "=", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        RowBox[{"2", " ", "m", " ", "g", " ", "R", " ", 
         RowBox[{"Cos", "[", 
          RowBox[{"\[Alpha]", "/", "2"}], "]"}]}], ",", "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0", ",", 
        RowBox[{"m", " ", "g", " ", "b"}]}], "}"}]}], "}"}]}], 
   "\[IndentingNewLine]", 
   RowBox[{"MatrixForm", "[", "KK", "]"}]}]}]], "Input",
 CellChangeTimes->{{3.7774909645964737`*^9, 3.7774909697520533`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"2", " ", "g", " ", "m", " ", "R", " ", 
      RowBox[{"Cos", "[", 
       FractionBox["\[Alpha]", "2"], "]"}]}], ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", 
     RowBox[{"b", " ", "g", " ", "m"}]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.7774909703888273`*^9}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      RowBox[{"2", " ", "g", " ", "m", " ", "R", " ", 
       RowBox[{"Cos", "[", 
        FractionBox["\[Alpha]", "2"], "]"}]}], "0"},
     {"0", 
      RowBox[{"b", " ", "g", " ", "m"}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.7774909703926897`*^9}]
}, Open  ]],

Cell[BoxData["\[IndentingNewLine]"], "Input",
 CellChangeTimes->{3.77749119033504*^9}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"MatrixForm", "[", 
   RowBox[{"KK", "-", 
    RowBox[{"\[Lambda]", " ", "MM"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.77749101108834*^9, 3.777491043737958*^9}, {
  3.777491208680254*^9, 3.777491229526512*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      RowBox[{
       RowBox[{
        RowBox[{"-", "2"}], " ", "m", " ", 
        SuperscriptBox["R", "2"], " ", "\[Lambda]"}], "+", 
       RowBox[{"2", " ", "g", " ", "m", " ", "R", " ", 
        RowBox[{"Cos", "[", 
         FractionBox["\[Alpha]", "2"], "]"}]}]}], 
      RowBox[{"b", " ", "m", " ", "R", " ", "\[Lambda]", " ", 
       RowBox[{"Cos", "[", 
        FractionBox["\[Alpha]", "2"], "]"}]}]},
     {
      RowBox[{"b", " ", "m", " ", "R", " ", "\[Lambda]", " ", 
       RowBox[{"Cos", "[", 
        FractionBox["\[Alpha]", "2"], "]"}]}], 
      RowBox[{
       RowBox[{"b", " ", "g", " ", "m"}], "-", 
       RowBox[{
        SuperscriptBox["b", "2"], " ", "m", " ", "\[Lambda]"}]}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{{3.7774910386634607`*^9, 3.777491044601266*^9}, {
  3.77749121496691*^9, 3.777491229967387*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"Det", "[", 
   RowBox[{"KK", "-", 
    RowBox[{"\[Lambda]", " ", "MM"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7774912398798523`*^9, 3.7774912527717857`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"-", "2"}], " ", "b", " ", "g", " ", 
   SuperscriptBox["m", "2"], " ", 
   SuperscriptBox["R", "2"], " ", "\[Lambda]"}], "+", 
  RowBox[{"2", " ", 
   SuperscriptBox["b", "2"], " ", 
   SuperscriptBox["m", "2"], " ", 
   SuperscriptBox["R", "2"], " ", 
   SuperscriptBox["\[Lambda]", "2"]}], "+", 
  RowBox[{"2", " ", "b", " ", 
   SuperscriptBox["g", "2"], " ", 
   SuperscriptBox["m", "2"], " ", "R", " ", 
   RowBox[{"Cos", "[", 
    FractionBox["\[Alpha]", "2"], "]"}]}], "-", 
  RowBox[{"2", " ", 
   SuperscriptBox["b", "2"], " ", "g", " ", 
   SuperscriptBox["m", "2"], " ", "R", " ", "\[Lambda]", " ", 
   RowBox[{"Cos", "[", 
    FractionBox["\[Alpha]", "2"], "]"}]}], "-", 
  RowBox[{
   SuperscriptBox["b", "2"], " ", 
   SuperscriptBox["m", "2"], " ", 
   SuperscriptBox["R", "2"], " ", 
   SuperscriptBox["\[Lambda]", "2"], " ", 
   SuperscriptBox[
    RowBox[{"Cos", "[", 
     FractionBox["\[Alpha]", "2"], "]"}], "2"]}]}]], "Output",
 CellChangeTimes->{3.777491255912216*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Solve", "[", 
   RowBox[{
    RowBox[{"%", "\[Equal]", "0"}], ",", "\[Lambda]"}], "]"}], 
  "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.777491261151774*^9, 3.7774912691740417`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"\[Lambda]", "\[Rule]", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"-", "4"}], " ", "g", " ", "R"}], "-", 
        RowBox[{"4", " ", "b", " ", "g", " ", 
         RowBox[{"Cos", "[", 
          FractionBox["\[Alpha]", "2"], "]"}]}], "-", 
        RowBox[{"\[Sqrt]", 
         RowBox[{"(", 
          RowBox[{
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{
              RowBox[{"4", " ", "g", " ", "R"}], "+", 
              RowBox[{"4", " ", "b", " ", "g", " ", 
               RowBox[{"Cos", "[", 
                FractionBox["\[Alpha]", "2"], "]"}]}]}], ")"}], "2"], "+", 
           RowBox[{"16", " ", 
            SuperscriptBox["g", "2"], " ", 
            RowBox[{"Cos", "[", 
             FractionBox["\[Alpha]", "2"], "]"}], " ", 
            RowBox[{"(", 
             RowBox[{
              RowBox[{
               RowBox[{"-", "3"}], " ", "b", " ", "R"}], "+", 
              RowBox[{"b", " ", "R", " ", 
               RowBox[{"Cos", "[", "\[Alpha]", "]"}]}]}], ")"}]}]}], 
          ")"}]}]}], ")"}], "/", 
      RowBox[{"(", 
       RowBox[{"2", " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{
           RowBox[{"-", "3"}], " ", "b", " ", "R"}], "+", 
          RowBox[{"b", " ", "R", " ", 
           RowBox[{"Cos", "[", "\[Alpha]", "]"}]}]}], ")"}]}], ")"}]}]}], 
    "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\[Lambda]", "\[Rule]", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"-", "4"}], " ", "g", " ", "R"}], "-", 
        RowBox[{"4", " ", "b", " ", "g", " ", 
         RowBox[{"Cos", "[", 
          FractionBox["\[Alpha]", "2"], "]"}]}], "+", 
        RowBox[{"\[Sqrt]", 
         RowBox[{"(", 
          RowBox[{
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{
              RowBox[{"4", " ", "g", " ", "R"}], "+", 
              RowBox[{"4", " ", "b", " ", "g", " ", 
               RowBox[{"Cos", "[", 
                FractionBox["\[Alpha]", "2"], "]"}]}]}], ")"}], "2"], "+", 
           RowBox[{"16", " ", 
            SuperscriptBox["g", "2"], " ", 
            RowBox[{"Cos", "[", 
             FractionBox["\[Alpha]", "2"], "]"}], " ", 
            RowBox[{"(", 
             RowBox[{
              RowBox[{
               RowBox[{"-", "3"}], " ", "b", " ", "R"}], "+", 
              RowBox[{"b", " ", "R", " ", 
               RowBox[{"Cos", "[", "\[Alpha]", "]"}]}]}], ")"}]}]}], 
          ")"}]}]}], ")"}], "/", 
      RowBox[{"(", 
       RowBox[{"2", " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{
           RowBox[{"-", "3"}], " ", "b", " ", "R"}], "+", 
          RowBox[{"b", " ", "R", " ", 
           RowBox[{"Cos", "[", "\[Alpha]", "]"}]}]}], ")"}]}], ")"}]}]}], 
    "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.777491270432343*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"lambda", "[", 
    RowBox[{"g_", ",", "R_", ",", "b_", ",", "\[Alpha]_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"-", "4"}], " ", "g", " ", "R"}], "-", 
      RowBox[{"4", " ", "b", " ", "g", " ", 
       RowBox[{"Cos", "[", 
        FractionBox["\[Alpha]", "2"], "]"}]}], "-", 
      RowBox[{"\[Sqrt]", 
       RowBox[{"(", 
        RowBox[{
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{
            RowBox[{"4", " ", "g", " ", "R"}], "+", 
            RowBox[{"4", " ", "b", " ", "g", " ", 
             RowBox[{"Cos", "[", 
              FractionBox["\[Alpha]", "2"], "]"}]}]}], ")"}], "2"], "+", 
         RowBox[{"16", " ", 
          SuperscriptBox["g", "2"], " ", 
          RowBox[{"Cos", "[", 
           FractionBox["\[Alpha]", "2"], "]"}], " ", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"-", "3"}], " ", "b", " ", "R"}], "+", 
            RowBox[{"b", " ", "R", " ", 
             RowBox[{"Cos", "[", "\[Alpha]", "]"}]}]}], ")"}]}]}], ")"}]}]}], 
     ")"}], "/", 
    RowBox[{"(", 
     RowBox[{"2", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"-", "3"}], " ", "b", " ", "R"}], "+", 
        RowBox[{"b", " ", "R", " ", 
         RowBox[{"Cos", "[", "\[Alpha]", "]"}]}]}], ")"}]}], 
     ")"}]}]}]}]], "Input",
 CellChangeTimes->{{3.777493223457262*^9, 3.777493250498488*^9}, {
   3.7774934460507383`*^9, 3.777493447886641*^9}, 3.777493479135456*^9}],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"lambda", "[", 
   RowBox[{"g", ",", "R", ",", "R", ",", "0"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.777493254617669*^9, 3.7774932621457863`*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{
    RowBox[{
     RowBox[{"-", "8"}], " ", "g", " ", "R"}], "-", 
    RowBox[{"4", " ", 
     SqrtBox["2"], " ", "g", " ", "R"}]}], 
   RowBox[{"4", " ", 
    SuperscriptBox["R", "2"]}]]}]], "Input",
 CellChangeTimes->{{3.777493492480846*^9, 3.777493493064433*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Simplify", "[", 
  RowBox[{"-", 
   FractionBox[
    RowBox[{
     RowBox[{
      RowBox[{"-", "8"}], " ", "g", " ", "R"}], "-", 
     RowBox[{"4", " ", 
      SqrtBox["2"], " ", "g", " ", "R"}]}], 
    RowBox[{"4", " ", 
     SuperscriptBox["R", "2"]}]]}], "]"}]], "Input",
 CellChangeTimes->{{3.777493497740385*^9, 3.777493503205085*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{
   RowBox[{"(", 
    RowBox[{"2", "+", 
     SqrtBox["2"]}], ")"}], " ", "g"}], "R"]], "Output",
 CellChangeTimes->{3.7774935037317743`*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"Lambda1nacho", "[", 
    RowBox[{"g_", ",", "R_", ",", " ", "b_", ",", " ", "\[Alpha]_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"g", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"b", " ", 
          RowBox[{"Cos", "[", 
           RowBox[{"\[Alpha]", "/", "2"}], "]"}]}], "+", "R"}], ")"}]}], "+", 
      
      RowBox[{
       RowBox[{"Sqrt", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"b", "^", "2"}], " ", 
          RowBox[{
           RowBox[{"Cos", "[", 
            RowBox[{"\[Alpha]", "/", "2"}], "]"}], "^", "2"}]}], "+", 
         RowBox[{"2", " ", "R", " ", "b", " ", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"Cos", "[", 
              RowBox[{"\[Alpha]", "/", "2"}], "]"}], "^", "3"}], "-", 
            RowBox[{"Cos", "[", 
             RowBox[{"\[Alpha]", "/", "2"}], "]"}]}], ")"}]}], "+", 
         RowBox[{"R", "^", "2"}]}], "]"}], "g"}]}], ")"}], "/", 
    RowBox[{"(", 
     RowBox[{"R", " ", "b", " ", 
      RowBox[{"(", 
       RowBox[{"2", "-", 
        RowBox[{
         RowBox[{"Cos", "[", 
          RowBox[{"\[Alpha]", "/", "2"}], "]"}], "^", "2"}]}], ")"}]}], 
     ")"}]}]}]}]], "Input",
 CellChangeTimes->{{3.777493874239718*^9, 3.777493877989003*^9}, {
   3.7774940728712397`*^9, 3.777494188479141*^9}, {3.777494317291218*^9, 
   3.777494320698556*^9}, 3.777495264425959*^9, {3.777549439193177*^9, 
   3.777549439277471*^9}}],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"Lambda2nacho", "[", 
    RowBox[{"g_", ",", "R_", ",", " ", "b_", ",", " ", "\[Alpha]_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"g", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"b", " ", 
          RowBox[{"Cos", "[", 
           RowBox[{"\[Alpha]", "/", "2"}], "]"}]}], "+", "R"}], ")"}]}], "-", 
      
      RowBox[{
       RowBox[{"Sqrt", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"b", "^", "2"}], " ", 
          RowBox[{
           RowBox[{"Cos", "[", 
            RowBox[{"\[Alpha]", "/", "2"}], "]"}], "^", "2"}]}], "+", 
         RowBox[{"2", " ", "R", " ", "b", " ", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"Cos", "[", 
              RowBox[{"\[Alpha]", "/", "2"}], "]"}], "^", "3"}], "-", 
            RowBox[{"Cos", "[", 
             RowBox[{"\[Alpha]", "/", "2"}], "]"}]}], ")"}]}], "+", 
         RowBox[{"R", "^", "2"}]}], "]"}], "g"}]}], ")"}], "/", 
    RowBox[{"(", 
     RowBox[{"R", " ", "b", " ", 
      RowBox[{"(", 
       RowBox[{"2", "-", 
        RowBox[{
         RowBox[{"Cos", "[", 
          RowBox[{"\[Alpha]", "/", "2"}], "]"}], "^", "2"}]}], ")"}]}], 
     ")"}]}]}]}]], "Input",
 CellChangeTimes->{{3.7774945673833323`*^9, 3.7774945808847933`*^9}, 
   3.777495267302451*^9, {3.77754944100467*^9, 3.777549441076824*^9}}],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"Lambda1nacho", "[", 
    RowBox[{"g", ",", "R", ",", " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"Sqrt", "[", "2", "]"}], "R"}], ")"}], ",", 
     RowBox[{"(", 
      RowBox[{"\[Pi]", "/", "2"}], ")"}]}], "]"}], 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.777494482657783*^9, 3.777494519563088*^9}, {
   3.777494818176393*^9, 3.7774948241851673`*^9}, 3.7774951891202517`*^9, 
   3.777495301782206*^9, {3.777495407905768*^9, 3.7774954300595007`*^9}, 
   3.7774954608577843`*^9, {3.777549369461049*^9, 3.777549371172065*^9}, {
   3.777549404180427*^9, 3.777549406500661*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 FractionBox[
  RowBox[{
   SqrtBox["2"], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"2", " ", "g", " ", "R"}], "+", 
     RowBox[{"g", " ", "R"}]}], ")"}]}], 
  RowBox[{"3", " ", 
   SuperscriptBox["R", "2"]}]]], "Input",
 CellChangeTimes->{{3.777549457655115*^9, 3.7775494641143827`*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SqrtBox["2"], " ", "g"}], "R"]], "Output",
 CellChangeTimes->{3.7775494749227858`*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"Lambda2nacho", "[", 
   RowBox[{"g", ",", "R", ",", " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"Sqrt", "[", "2", "]"}], "R"}], ")"}], ",", 
    RowBox[{"(", 
     RowBox[{"\[Pi]", "/", "2"}], ")"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.777495194824963*^9, 3.777495195817603*^9}, 
   3.777495306810773*^9, {3.777495417394781*^9, 3.77749544933594*^9}, {
   3.777549390360244*^9, 3.777549399117652*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 FractionBox[
  RowBox[{
   SqrtBox["2"], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"2", " ", "g", " ", "R"}], "-", 
     RowBox[{"g", " ", "R"}]}], ")"}]}], 
  RowBox[{"3", " ", 
   SuperscriptBox["R", "2"]}]]], "Input",
 CellChangeTimes->{{3.777549470617546*^9, 3.777549471211941*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SqrtBox["2"], " ", "g"}], 
  RowBox[{"3", " ", "R"}]]], "Output",
 CellChangeTimes->{3.777549471731969*^9}]
}, Open  ]]
},
WindowSize->{1061, 525},
WindowMargins->{{Automatic, 50}, {Automatic, 23}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 829, 23, 77, "Input"],
Cell[1412, 47, 603, 19, 45, "Output"],
Cell[2018, 68, 1027, 30, 80, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3082, 103, 575, 16, 77, "Input"],
Cell[3660, 121, 374, 11, 45, "Output"],
Cell[4037, 134, 792, 22, 70, "Output"]
}, Open  ]],
Cell[4844, 159, 86, 1, 55, "Input"],
Cell[CellGroupData[{
Cell[4955, 164, 272, 6, 55, "Input"],
Cell[5230, 172, 1376, 36, 80, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6643, 213, 221, 5, 55, "Input"],
Cell[6867, 220, 1038, 29, 45, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7942, 254, 229, 6, 55, "Input"],
Cell[8174, 262, 2925, 83, 148, "Output"]
}, Open  ]],
Cell[11114, 348, 1605, 44, 150, "Input"],
Cell[12722, 394, 207, 4, 55, "Input"],
Cell[12932, 400, 321, 10, 66, InheritFromParent],
Cell[CellGroupData[{
Cell[13278, 414, 364, 11, 66, InheritFromParent],
Cell[13645, 427, 179, 6, 61, "Output"]
}, Open  ]],
Cell[13839, 436, 1557, 43, 99, "Input"],
Cell[15399, 481, 1435, 41, 77, "Input"],
Cell[16837, 524, 673, 15, 77, "Input"],
Cell[CellGroupData[{
Cell[17535, 543, 310, 10, 66, InheritFromParent],
Cell[17848, 555, 128, 4, 55, "Output"]
}, Open  ]],
Cell[17991, 562, 473, 11, 55, "Input"],
Cell[CellGroupData[{
Cell[18489, 577, 308, 10, 66, InheritFromParent],
Cell[18800, 589, 149, 5, 55, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
