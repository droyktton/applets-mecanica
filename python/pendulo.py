#paquetes requeridos: matplotlib, scipy, numpy

from scipy.integrate import ode
import numpy as np
import matplotlib.pyplot as plt
import time

# parametros del pendulo:
g = 9.8 #m/s^2
l = 1.0 #m

# ecuacion del pendulo:
# theta_dotdot = -g*sin(theta) / l

# sistema de primer orden:
# theta_dot = omega
# omega_dot = - g * sin(theta) / l

# llamamos x = [theta,omega]
# escribimos el sistema como: x_dot = func(t,x)
def func(t,x):
    return [x[1],-(g/l)*np.sin(x[0])]

# condiciones iniciales
theta0 = np.deg2rad(10) #theta en radianes
omega0 = np.deg2rad(0) #omega en rad/s
x0 = [theta0,omega0]
t0 = 0 #tiempo inicial

# definimos el problema ODE
r = ode(func)
r.set_initial_value(x0,t0)

# parametros de simulacion
t1 = 10 #limite de tiempo 
dt = 0.1 #paso de simulacion

n = (int(t1/dt)+1) #cantidad de pasos de simulacion
solucion = [x0]*n
tiempos = [t0]*n
i = 0
while r.successful() and r.t < t1:
    x = r.integrate(r.t+dt)
    solucion[i] = x
    tiempos[i] = r.t+dt
    i += 1
    
    #pasamos la soluciona radianes y la metemos en un vector numpy para plotear
    x = np.rad2deg(np.array(solucion[:i]))
    
    plt.figure(1)
    plt.plot(tiempos[:i],x[:,0],'b')
    plt.plot(tiempos[:i],x[:,1],'r')
    plt.xlabel("tiempo (s)")
    plt.legend(["angulo (deg)","velocidad angular (deg/s)"])

    plt.draw()
    plt.pause(0.001) #necesario para que matplotlib actualice el dibujo

    time.sleep(dt)

